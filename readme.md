# continuous-delivery

## Create directory on host:
- /opt/data/nexus
- /opt/data/jenkins
- /opt/data/gitlab/gitlab
- /opt/data/gitlab/postgresql
- /opt/data/gitlab/redis

## Make writable:
- chmod a+w /opt/data/nexus
- chmod a+w /opt/data/jenkins
- chmod a+w -R /opt/data/gitlab

## Add to preference of Docker so can be shared

- Nexus can be reached at: localhost:8081  user: admin/admin123
- Jenkins can be readched at: localhost:8080 user:admin/admin123
- GitLab can be reached at: localhost:8082 user:root/admin123
